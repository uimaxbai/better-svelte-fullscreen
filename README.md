# Svelte FullScreen

Component that performs fullscreen in DOM Elements, but it is more up-to-date.

### Original project can be found [here](https://github.com/andrelmlins/svelte-fullscreen)

## Installation

```
npm i better-svelte-fullscreen
// OR
yarn add better-svelte-fullscreen
```

<em>Note: to use this library in sapper, install as devDependency. See the [link](https://github.com/sveltejs/sapper-template#using-external-components).</em>


Local demo:

```
git clone https://gitlab.com/uimaxbai/better-svelte-fullscreen/
cd svelte-fullscreen
yarn install && yarn start
```

## Examples

An example of how to use the library:

```svelte
<script>
  import Fullscreen from "better-svelte-fullscreen";
</script>

<style>
  div {
    background-color: red;
    width: 120px;
    height: 120px;
  }
</style>

<Fullscreen let:onRequest let:onExit>
  <div>
    <button on:click={() => onRequest()}>FullScreen</button>
    <button on:click={() => onExit()}>Screen</button>
  </div>
</Fullscreen>
```

## Properties

Raw component props (before transform):

| Prop   | Type | Description    |
| ------ | ---- | -------------- |
| change | func | Call in change |
| error  | func | Call in error  |

## Slot Properties

Raw component props (before transform):

| Prop      | Type | Description                |
| --------- | ---- | -------------------------- |
| onToggle  | func | Call for fullscreen toggle |
| onExit    | func | Call for fullscreen exit   |
| onRequest | func | Call for fullscreen enter  |

## Browsers Support

You can see the list of supported browsers [here](https://caniuse.com/fullscreen)

![Browsers support](assets/browser-support.png)

## License

Better Svelte Fullscreen is open source software also [licensed as MIT](https://gitlab.com/uimaxbai/better-svelte-fullscreen/-/blob/master/LICENSE).
